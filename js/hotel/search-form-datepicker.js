var DateRange;

(function($) {

    DateRange = function(idFrom, idTo)
    {
        var formatSubmit = 'yy-mm-dd';
        var formatDisplay  = 'D d M yy';

        function resolveOptionValuesByType(id, type)
        {
            var values = [];

            $(id).children('option').each(function(){
                if ($(this).data('night-type') == type) {
                    values.push($(this).val());
                }
            });

            return values;
        }

        function initDatePicker(id)
        {
            var elementSelect = $(id);

            var startDate = $.datepicker.parseDate(formatSubmit, elementSelect.children('option:first-child').val());
            var endDate = $.datepicker.parseDate(formatSubmit, elementSelect.children('option:last-child').val());

            var defaultDate = $.datepicker.parseDate(formatSubmit, elementSelect.children('option:selected').val());

            elementSelect.css('display', 'none');

            $('<input />')
                .attr({
                    'id': id.replace('#', '') + '_input',
                    'type': 'text',
                    'value': $.datepicker.formatDate(formatDisplay, defaultDate),
                    'class': elementSelect.attr('class')
                })
                .insertAfter(elementSelect);

            return $(id + '_input').datepicker({
                dateFormat: formatDisplay,
                minDate: new Date(startDate[2], startDate[1] - 1, startDate[0]),
                maxDate: new Date(endDate[2], endDate[1] - 1, endDate[0]),
                setDate: new Date(defaultDate[2], defaultDate[1] - 1, defaultDate[0]),
                beforeShow: function (input, datepicker) {
                    $(input).closest('aside').append(datepicker.dpDiv);
                },
                beforeShowDay: function (date) {
                    if ($.inArray($.datepicker.formatDate(formatSubmit, date), datesCore) > -1) {
                        return [true, 'core-night', 'Core Night'];
                    } else if ($.inArray($.datepicker.formatDate(formatSubmit, date), datesExtended) > -1) {
                        return [true, 'extended-night', 'Extended Night'];
                    } else {
                        return [false, '', ''];
                    }
                },
                onSelect: function (valueDisplay, datepicker) {
                    var valueSubmit = $.datepicker.formatDate(formatSubmit, new Date(
                        datepicker.selectedYear,
                        datepicker.selectedMonth,
                        datepicker.selectedDay
                    ));

                    elementSelect.children('option').removeAttr('selected');
                    elementSelect.children('option').filter(function () {
                        return $(this).val() == valueSubmit;
                    }).attr('selected', true);

                    if (valueDisplay != datepicker.lastVal) {
                        $(this).change();
                    }
                }
            });
        }

        var datesCore = resolveOptionValuesByType(idFrom, 'core');
        var datesExtended = resolveOptionValuesByType(idFrom, 'extended');

        var datepickerFrom = initDatePicker(idFrom);
        var datepickerTo   = initDatePicker(idTo);

        datepickerFrom.on('change', function() {
            datepickerTo.datepicker('option', 'minDate', $.datepicker.parseDate(formatDisplay, this.value));
            setTimeout(function() { datepickerTo.datepicker('show'); }, 50);
        });

        datepickerTo.on('change', function() {
            datepickerFrom.datepicker('option', 'maxDate', $.datepicker.parseDate(formatDisplay, this.value));
        });
    };

})(jQuery);

new DateRange('#date_arrival', '#date_departure');
