var $ = jQuery.noConflict();
$(document).ready(function() {

    $('.search-results-item-details').each(function() {
        var descHeight = $(this).outerHeight();
        if(descHeight > 210) {
            $(this).css({
                'height' : 170,
                'padding-bottom' : 40
            });
            $(this).children('.description-toggle').click(function(e) {
                e.preventDefault();
                if($(this).parents('.search-results-item-details').hasClass('open')) {
                    $(this).parents('.search-results-item-details').animate({
                        'height' : 170
                    }, { queue:false, duration:500 });
                    $(this).parents('.search-results-item-details').removeClass('open');
                    $(this).children('span').children('em').html('Read More');
                } else {
                    $(this).parents('.search-results-item-details').animate({
                        'height' : descHeight
                    }, { queue:false, duration:500 });
                    $(this).parents('.search-results-item-details').addClass('open');
                    $(this).children('span').children('em').html('Read Less');
                }
            });
        } else {
            $(this).children('.description-toggle').hide();
        }
    });

    $('.hotel-gallery-thumbs').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 10,
        slideWidth: 110,
        pager: false,
        prevText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-left fa-stack-1x fa-inverse"></i></span>',
        nextText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x fa-inverse"></i></span>',
        hideControlOnEnd: true,
        infiniteLoop: false
    });

    $('.overlay-trigger').click(function(e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('html').css('overflow', 'hidden');
        $('.overlay-cover, #' + target).show().css('visibility', 'visible');
    });

    $('.overlay-close').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'auto');
        $('.overlay-cover, .overlay-wrap').hide();
    });

    $('#payment_cards input').on('change', function() {
        var selected = $('input:checked', '#payment_cards');
        console.log(selected);
        $('form .payment-card.active, form .payment-card-new.active').removeClass('active');
        $(selected).parents('.payment-card, .payment-card-new').toggleClass('active');
    });

    $('.checkbox-list li').append('<span class="check"></span>');

    $('.checkbox-list li').click(function(e) {
        var container = $(this).parents('.checkbox-list');
        $(container).find('input:radio').removeAttr('checked');
        $(this).children('input:radio').attr('checked', 'checked');

        if($(this).children('input:checkbox').hasClass('checked')) { 
            console.log('yes');
            $(this).children('input:checkbox').removeAttr('checked').removeClass('checked');
        } else {
            console.log('no');
            $(this).children('input:checkbox').attr('checked', 'checked').addClass('checked');
        }
    });

});

enquire
    .register("screen and (max-width:65em) and (orientation: landscape), screen and (min-width: 65em)", function() { 
        $(document).ready(function() {
            var headerHeight = $('header').outerHeight();
            var footerHeight = $('footer').outerHeight();
            $('.form-splash:not(.landing-splash)').css('padding-top', (headerHeight + 80));
            $('.header-fix').css({
                'padding-top' : headerHeight,
                'padding-bottom' : footerHeight
            });

            $('.hotel-gallery').bxSlider({
                pagerCustom: '.hotel-gallery-thumbs',
                controls: false,
                mode: 'fade'
            });

            $('.room-dates-list').bxSlider({
                minSlides: 3,
                maxSlides: 3,
                moveSlides: 1,
                slideWidth: 150,
                slideMargin: 10,
                pager: false,
                prevText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-left fa-stack-1x fa-inverse"></i></span>',
                nextText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x fa-inverse"></i></span>',
                hideControlOnEnd: true,
                infiniteLoop: false
            }); 

            $('.search-results-item-rooms-list').bxSlider({
                minSlides: 3,
                maxSlides: 3,
                moveSlides: 1,
                slideWidth: 270,
                slideMargin: 10,
                pager: false,
                prevText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-left fa-stack-1x fa-inverse"></i></span>',
                nextText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x fa-inverse"></i></span>',
                hideControlOnEnd: true,
                infiniteLoop: false
            });
        });
    }, true)
    .register("only screen and (max-width : 65em) and (orientation: portrait)", function() { 
        $(document).ready(function() {
            $('.hotel-gallery').bxSlider({
                minSlides: 2,
                maxSlides: 3,
                moveSlides: 1,
                slideWidth: 250,
                slideMargin: 20,
                pager: false,
                prevText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-left fa-stack-1x fa-inverse"></i></span>',
                nextText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x fa-inverse"></i></span>',
                hideControlOnEnd: true,
                infiniteLoop: false
            });
            $('.search-results-item-rooms-list').bxSlider({
                pager: false,
                prevText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-left fa-stack-1x fa-inverse"></i></span>',
                nextText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x fa-inverse"></i></span>',
                hideControlOnEnd: true,
                infiniteLoop: false,
                adaptiveHeight: true
            });
            $('.room-dates-list').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                moveSlides: 1,
                slideWidth: 200,
                slideMargin: 0,
                pager: false,
                prevText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-left fa-stack-1x fa-inverse"></i></span>',
                nextText: '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x fa-inverse"></i></span>',
                hideControlOnEnd: true,
                infiniteLoop: false,
                adaptiveHeight: true
            });
        });
    });

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
